'use strict';

export const generateGrid = function (variables, wordLength = 5) {
  variables.wordLength = wordLength;

  const gameGrid = document.querySelector('.row-container');

  //  Deletes existing grid
  gameGrid.innerHTML = '';

  // Renders and display tooltip
  const tooltip = `<div id="tooltip-container"></div>`;
  gameGrid.insertAdjacentHTML('afterbegin', tooltip);

  //  Renders a single row
  const gameRowTemplate = function (id) {
    return `<div class="game-row flex-container" id="game-row-${
      id + 1
    }"></div>`;
  };

  //  Renders a single cell
  const gameCellTemplate = function (cellNo) {
    return `<div class="game-cell" id="${cellNo}"></div>`;
  };

  //  Loops and adds multiple rows and cells
  for (let i = 0; i < variables.gameRows; i++) {
    const newRow = gameRowTemplate(i);

    //  Display row
    gameGrid.insertAdjacentHTML('beforeend', newRow);

    for (let j = 0; j < variables.wordLength; j++) {
      const latestRow = document.querySelector(`#game-row-${i + 1}`);
      // Display cell
      latestRow.insertAdjacentHTML('afterbegin', gameCellTemplate());
    }
  }
  return true;
};
