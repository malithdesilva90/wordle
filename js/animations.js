'use strict';

//
// IMPORTS
//
import { allCells, cellCount, currentVariables } from './script.js';

//
//  DEFINITIONS
//
let tooltipCont = document.querySelector('#tooltip-container');

//
//  HIGHLIGHTING ACTIONS
//

export const toggleDarkMode = function () {
  const allSelectable = document.querySelectorAll('.cm-toggle');

  allSelectable.forEach((el) => {
    if (el.classList.contains('cm-inverse')) {
      el.classList.toggle('dark-mode--inverse');
    } else {
      el.classList.toggle('dark-mode--black');
    }
  });
};

export const paintGray = function () {
  for (let i = currentVariables.wordLength; i > 0; i--) {
    allCells[cellCount - i].classList.toggle('gray-cell');
  }
};

export const paintColor = function (selection, action, color) {
  if (action == 'add') selection.classList.add(`${color}-cell`);
  if (action == 'remove') selection.classList.remove(`${color}-cell`);
};

export const fireTooltip = function (msg, cssClass, duration) {
  tooltipCont = document.querySelector('#tooltip-container');
  const tip = `<div class="tooltip ${cssClass}">${msg}</div>`;

  tooltipCont.innerHTML = '';
  tooltipCont.insertAdjacentHTML('afterbegin', tip);
  setTimeout(function () {
    tooltipCont.innerHTML = '';
  }, duration);
};

export const keyHighlightHint = function (el) {
  //  Selects relevant keyboard key
  const cell = document.querySelector(`[data-value=${el}]`);

  //  Guard Clause: If doesn't exist, break execution. Unlikely
  if (!cell) return null;

  //  Fires highlight animation
  cell.classList.add('hint-highlight');

  //  Removes highlight animation after delay
  setTimeout(() => {
    cell.classList.remove('hint-highlight');
  }, 1500);
};
