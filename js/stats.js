export const calculateStreak = function () {
  const stat = JSON.parse(localStorage.getItem('stats'));

  stat.maxStreak = maxStreak(stat.results);
  stat.curStreak = curStreak(stat.results);

  localStorage.setItem('stats', JSON.stringify(stat));

  const wins = document.getElementById('label-stat--wins');
  const losses = document.getElementById('label-stat--losses');
  const cur_streak = document.getElementById('label-stat--streak');
  const max_streak = document.getElementById('label-stat--maxStreak');
  const allProgressBars = document.querySelectorAll('.progress-bar');

  const max = Math.max(...Object.values(stat.rows));
  allProgressBars.forEach((el, i) => {
    el.textContent = stat.rows[i + 1];
    el.style.setProperty('width', `calc(100% * ${stat.rows[i + 1] / max})`);
  });

  wins.textContent = stat.wins;
  losses.textContent = stat.losses;
  cur_streak.textContent = stat.curStreak;
  max_streak.textContent = stat.maxStreak;

  return stat;
};

export const curStreak = function (arr) {
  let streak = 0;

  for (let i = arr.length - 1; i >= 0; i--) {
    if (arr[i] === 'W') {
      streak++;
    } else {
      break;
    }
  }

  return streak;
};

export const maxStreak = function (arr) {
  let i,
    temp,
    streak,
    length = arr.length,
    highestStreak = 0;

  for (i = 0; i < length; i++) {
    // check the value of the current entry against the last
    if (temp != '' && temp == arr[i]) {
      // it's a match
      streak++;
    } else {
      // it's not a match, start streak from 1
      streak = 1;
    }

    // set current letter for next time
    temp = arr[i];

    // set the master streak var
    if (streak > highestStreak) {
      highestStreak = streak;
    }
  }
  return highestStreak;
};
