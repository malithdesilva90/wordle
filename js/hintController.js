'use strict';

import {
  cellCount,
  allLetters,
  currentVariables,
  wordComplete,
  rowCompleted,
  hiddenWordArr,
} from './script.js';

let hintArr;

export const hintFactory = function () {
  //  Activates hint functionality only after first word is completed
  const hintActivate =
    cellCount >= currentVariables.wordLength &&
    (cellCount / currentVariables.wordLength > 1 || !wordComplete);

  //  Guard Clause: If not, does nothing
  if (!hintActivate) return null;

  let enteredLetters;

  //  Creates a Set of all the valid entered letters
  if (hintActivate) {
    enteredLetters = new Set(
      allLetters.slice(0, rowCompleted * currentVariables.wordLength)
    );
  }

  // Checks aforementioned Set against the Hidden Word and filters out matching letters
  hintArr = hiddenWordArr.filter((el) => !enteredLetters.has(el));

  //  Returns an array of letters yet to be discovered
  return hintArr;
};
