'use strict';

//
//  IMPORTS
//
import { dictionary } from '../data/sample-dict.js';
import { callModal } from './modal.js';
import { calculateStreak } from './stats.js';
import { generateGrid } from './buildGrid.js';
import { variables as curV } from './gameVariables.js';
import { hintFactory } from './hintController.js';
import {
  toggleDarkMode,
  paintColor,
  paintGray,
  fireTooltip,
  keyHighlightHint,
} from './animations.js';

//
//  DEFINITIONS
//
const keyboard = document.querySelector('.keyboard-container');

const gameID = document.querySelector('#game-id');
export let allCells = document.querySelectorAll('.game-cell');
const allKeys = document.querySelectorAll('.key-cell');
const lightDarkBtn = document.querySelector('#light-dark--btn');
const gameLengthInput = document.querySelector('#game-length--input');
const body = document.querySelector('body');
const hintBtn = document.querySelector('#hint-btn');
const hintRem = document.querySelector('#hint-rem');

//
// VARIABLES
//
export let cellCount = 0;
export let allLetters = [];
export let wordComplete = false;
export let wordEmpty = true;
export let rowCompleted = 0;
export let currentVariables = curV;
export let hiddenWord;
export let hiddenWordArr;
let remHints = 3;

currentVariables.totalLetters =
  currentVariables.gameRows * currentVariables.wordLength;

export const stats =
  localStorage.getItem('stats') ||
  localStorage.setItem(
    'stats',
    JSON.stringify({
      results: [],
      wins: 0,
      losses: 0,
      rows: {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
      },
    })
  );

//
// INITIALIZATION
//

const startGame = function () {
  // Renders the new game based on selected word lenght
  generateGrid(currentVariables, currentVariables.wordLength);

  const allCells = document.querySelectorAll('.game-cell');

  //  Selects the Hidden Word from dictionary for when the game actually starts
  hiddenWord =
    dictionary[currentVariables.wordLength][
      Math.floor(Math.random() * dictionary[currentVariables.wordLength].length)
    ];

  hiddenWordArr = hiddenWord.split('');

  console.log(hiddenWord);

  // Sets remaining hints text to default
  hintRem.textContent = `${currentVariables.startHints} Hints remaining`;

  // Displays the current Game ID
  gameID.textContent = `Game ID: ${currentVariables.wordLength}-${dictionary[
    currentVariables.wordLength
  ].indexOf(hiddenWord)}`;

  //  Adds dark mode related classes to all game cells and keys
  allCells.forEach((el) => el.classList.add('cm-toggle', 'cm-inverse'));
  allKeys.forEach((el) => el.classList.add('cm-toggle', 'cm-inverse'));
};

//
// MAIN ACTIONS
//

// Checks entered full-length word against dictionary. Only checks for existence.
const checkDictionary = function (word) {
  // WORD EXISTS
  if (dictionary[currentVariables.wordLength].includes(word)) {
    // Resets variables in preparation for next row
    wordComplete = false;
    wordEmpty = true;
    rowCompleted++;
    paintGray(word);
    checkLetters(word);

    return true;
  } // WORD DOESN'T EXIST
  else {
    fireTooltip('🤐 Not a valid word!', '', 1500);
    return false;
  }
};

const checkLetters = function (word) {
  const arr = word.split('');

  arr.forEach((el, i) => {
    const correctLetter = hiddenWord.includes(el);
    const correctPosition = hiddenWord.indexOf(el, i);

    let count = 0;
    let position = hiddenWord.indexOf(el);
    while (position !== -1) {
      count++;
      position = hiddenWord.indexOf(el, position + 1);
    }

    const key = document.querySelector(`[data-value=${el}]`);

    //  If letter is correct but in the wrong position
    if (correctLetter && (correctPosition !== i || count !== 1)) {
      paintColor(
        allCells[cellCount - currentVariables.wordLength + i],
        'add',
        'yellow'
      );
      paintColor(key, 'add', 'yellow');
    }

    //  If letter is correct and in the right position
    if (correctLetter && correctPosition == i) {
      paintColor(
        allCells[cellCount - currentVariables.wordLength + i],
        'remove',
        'yellow'
      );
      paintColor(
        allCells[cellCount - currentVariables.wordLength + i],
        'add',
        'green'
      );
      paintColor(key, 'remove', 'yellow');
      paintColor(key, 'add', 'green');
    }

    // If letter is not correct
    if (!correctLetter) paintColor(key, 'add', 'shaded');
  });

  // If word is completely correct
  if (word === hiddenWord) {
    // Display victory tooltip
    fireTooltip(
      '🎉 Winner \n A new game will start in 3 seconds!',
      'win-tooltip',
      3000
    );

    //  Updates stats
    const stat = JSON.parse(localStorage.getItem('stats'));
    stat.results.push('W');
    stat.wins++;

    const winningRow =
      Math.floor(allLetters.length) / currentVariables.wordLength;

    stat.rows[winningRow] = stat.rows[winningRow] + 1;
    localStorage.setItem('stats', JSON.stringify(stat));

    // Resets game after a delay
    return setTimeout(resetGame, 3000);
  }

  // If no more attempts remain
  if (cellCount >= currentVariables.totalLetters) {
    // Display loss tooltip
    fireTooltip(
      `😭 You lost! \n The word was ${hiddenWord}\n A new game will start in 4 seconds!`,
      'lose-tooltip',
      4000
    );

    // Updates stats
    const stat = JSON.parse(localStorage.getItem('stats'));
    stat.results.push('L');
    stat.losses++;
    localStorage.setItem('stats', JSON.stringify(stat));

    // Resets game after a delay
    return setTimeout(resetGame, 4000);
  }
};

//  Renders and displays entered letter
const displayLetter = function (letter) {
  if (cellCount >= currentVariables.totalLetters) return null;
  allLetters.push(letter.toUpperCase());

  allCells[cellCount].textContent = letter.toUpperCase();
  cellCount++;
  wordEmpty = false;
};

//  Deletes existing letter
const deleteLetter = function () {
  wordComplete == false;
  allLetters.pop();
  allCells[cellCount - 1].textContent = '';
  cellCount--;
  if (cellCount % currentVariables.wordLength == 0) wordEmpty = true;
};

//  Common hanlder for all input types including mouse clicks and keyboard strokes
const inputHandler = function (input) {
  //  Backspace key
  if (input === 'Backspace' && !wordEmpty) deleteLetter();

  //  Alphabet keys
  if (/[a-zA-Z]/.test(input) && input.length == 1 && !wordComplete)
    displayLetter(input);

  //  Enter key
  //  Invalid length
  if (input === 'Enter' && cellCount % currentVariables.wordLength !== 0) {
    fireTooltip('🙄 Too short of a word!', '', 3000);
  }

  //  Valid length
  if (
    input === 'Enter' &&
    cellCount % currentVariables.wordLength == 0 &&
    !wordEmpty
  ) {
    console.log('Checking word...please wait!');

    //  Checks entered word against dictionary
    checkDictionary(allLetters.slice(-currentVariables.wordLength).join(''));
  }

  //  Intermediate variable for use in other blocks
  cellCount % currentVariables.wordLength == 0 && !wordEmpty
    ? (wordComplete = true)
    : (wordComplete = false);

  // Disables changing word length while game is in progress
  if (allLetters.length >= currentVariables.wordLength && !wordComplete)
    gameLengthInput.disabled = true;
};

//
//  EVENT LISTENERS
//

// Clicks on virtual keyboard
keyboard.addEventListener('click', function (e) {
  if (e.target.classList.contains('key-cell')) {
    allCells = document.querySelectorAll('.game-cell');
    inputHandler(e.target.dataset.value);
  }
});

//  All keystrokes
document.addEventListener('keydown', function (e) {
  allCells = document.querySelectorAll('.game-cell');
  inputHandler(e.key);
});

// Dark-mode Button
lightDarkBtn.addEventListener('click', function (e) {
  e.preventDefault();
  toggleDarkMode();
});

// Word length input Button
gameLengthInput.addEventListener('change', function (e) {
  e.preventDefault();

  const newLength = +e.target.value;

  //  If word length is not valid
  if (
    newLength < currentVariables.minLength ||
    newLength > currentVariables.maxLength
  ) {
    fireTooltip('Word length is not valid', '', 5000);
  }
  // If valid word length
  else {
    currentVariables.wordLength = newLength;
    currentVariables.totalLetters =
      currentVariables.gameRows * currentVariables.wordLength;

    // Renders new grid and resets game
    generateGrid(currentVariables, newLength);
    resetGame();
  }
});

//  Get hint button
hintBtn.addEventListener('click', function (e) {
  e.preventDefault();

  // Gets an array of undiscovered letters
  const hintPool = hintFactory();

  //  Highlights a single RANDOM undiscovered letter on the virtual keyboard
  if (hintPool && remHints) {
    keyHighlightHint(hintPool[Math.floor(Math.random() * hintPool.length)]);
    remHints--;
    hintRem.textContent = `${remHints} Hints remaining`;
  }
});

const resetGame = function () {
  //  Removes all values and colors from game cells
  allCells.forEach((el) => {
    el.textContent = '';
    el.classList.remove('gray-cell', 'green-cell', 'yellow-cell');
  });

  //  Removes all colors from key cells
  allKeys.forEach((el) =>
    el.classList.remove('shaded-cell', 'green-cell', 'yellow-cell')
  );

  //  Resets all variables
  cellCount = 0;
  allLetters = [];
  wordComplete = false;
  wordEmpty = true;
  rowCompleted = 0;
  gameLengthInput.disabled = false;
  remHints = currentVariables.startHints;
  hintRem.textContent = `${remHints} Hints remaining`;

  //  Updates stats and starts a new game
  calculateStreak();
  startGame();
};

//  Starts the game on first run
window.onload = function (e) {
  startGame();
};

// Brings up modal window
callModal();

//  TODO: Fix grid length CSS bug
