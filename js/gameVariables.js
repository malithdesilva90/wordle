export const variables = {
  wordLength: 5,
  gameRows: 6,
  minLength: 4,
  maxLength: 8,
  startHints: 3,
};
